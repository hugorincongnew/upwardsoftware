ActiveAdmin.register ContactMessage do
  menu parent: "Notifications"
  permit_params :name, :email, :phone, :message

  index do
    selectable_column
    id_column
    column :name
    column :email
    actions
  end

  filter :name
  filter :email
  filter :phone

  form do |f|
    f.inputs "Contact Messages Details" do
      f.input :name
      f.input :email
      f.input :phone
      f.input :message
    end
    f.actions
  end
end