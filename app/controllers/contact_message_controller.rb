class ContactMessageController < ApplicationController
  
  def create
    @cm = ContactMessage.new(contact_message_params)
    if @cm.save
      redirect_to "/"
    else
      redirect_to "/"
    end    
  end
  
  def contact_message_params
    params.permit(:name, :email, :phone, :message)
  end    
  
end
